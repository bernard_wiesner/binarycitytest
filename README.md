In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser. It will aslo launch a MongoDB instance for the backend.

The web app was made using _React.js_ and _Javascript_, with a _MangoDB_ backend using _node.js_.

## What was not completed

- Clients and Contacts cannot be unlinked with a link as per the specification. I ran out of time and could not get around to implementing this.
- Finding of clients and contacts is performed on the data retrieved from the db locally so now querry is performed for these actions.
- I did not abstract the tables to a single table object, and so each table has its own implementation of a table
- Codes cannot have codes that need letters to be incremented twice, for example: if their is a FNB999 and FNC999 and the user wishes to add a new FNB then the code would only be increment from FNB->FNC and not FNB->FND

## Known issues

- I could not finish making data update on the fly, again due to time constraints, and so it may be required to reload the page or switch between client and contact tabs for the changes to be reflected on the page.
- The startup time of the database is quite long and as such there may be no data available when the page is loaded. Kindly wait a few seconds before trying to create clients or contacts as doing so may cause unstable behaviour.
- Some fields do not reset correctly one data submission
- If you leave a space at the end of the client it might cause undifend to appear in code generated for the client. I forgot to sanitize the input for this specific use case.
- I did not add full documentation just a few inline comments to explain whats going on a high level

## Extra notes

- I openend the database to all IP addresses so you should be able to access it. Otherwise let me know and I will try and sort it out.
- I have no experience with MongoDB prior to this test and learned it while I was doing this assignment.
- I also only very recently started doing web development again and am currently learning React for another project I am working on. Thus the reason I asked if I could use it as it was what I was most comfortable with.

// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our client data base's data structure
const ClientDataSchema = new Schema(
  {
    id: String,
    name: String,
    code: String,
    num: Number,
  },
  { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("ClientData", ClientDataSchema, "clients");

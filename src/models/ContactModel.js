// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our contact data base's data structure
const ContactDataSchema = new Schema(
  {
    id: String,
    name: String,
    surname: String,
    email: String,
    codes: Array,
    num: Number,
  },
  { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("ContactData", ContactDataSchema, "contacts");

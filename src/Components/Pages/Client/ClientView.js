import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import ContactList from "./ContactFind";
import ClientForm from "./ClientForm";
import ClientList from "./ClientList";
import LinkContact from "./LinkContact";

// The main view for clients
export default class ClientView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      isNew: false,
      client: {},
      message: "",
      code: "",
      clients: props.clients,
    };
  }

  componentWillReceiveProps(props) {
    this.setState({ props });
  }

  // calls the parent function in App.js to write a client to the db
  putDataToDB = (name, code) => {
    var clients = this.state.clients;
    this.props.addClient(name, code);
    // clients.push({ name, code, num: 0 });
    this.setState({ clients });
    this.setState({ message: "", isNew: false });
  };

  // calls the function in the parent in App.js to find the contacts associated with a client
  findClient = (name, code) => {
    console.log("Looking for client");
    for (var i = 0; i < this.state.clients.length; i++) {
      var client = this.state.clients[i];
      console.log(client.code);
      if (client.name === name) {
        var contacts = this.props.getContacts(client.code);
        console.log(contacts);
        this.setState({ client, contacts: contacts });
        return client.code;
      }
    }
    console.log("Not found");
    this.setState({
      isNew: true,
      message: "Client not found, click save to create one",
    });
    return null;
  };

  // The JSX to render
  render() {
    return (
      <div>
        <h1>Client View</h1>
        <h3> All Clients</h3>
        <ClientList list={this.state.clients.sort(compare)} />
        <hr></hr>
        <h1>Client Form</h1>
        <Tabs>
          <TabList>
            <Tab>General</Tab>
            <Tab>Contacts</Tab>
            <Tab>Link</Tab>
          </TabList>

          <TabPanel>
            <ClientForm
              addClient={this.putDataToDB}
              findClient={this.findClient}
              isNew={this.state.isNew}
              message={this.state.message}
              code={this.state.code}
              generateCode={this.props.generateCode}
            />
          </TabPanel>
          <TabPanel>
            <ContactList list={this.state.contacts} />
          </TabPanel>
          <TabPanel>
            <LinkContact
              client={this.state.client}
              linkContact={this.props.linkContact}
              message={this.props.linkMessage}
            ></LinkContact>
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

// Function to order the clients by name in asc. order
function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const bandA = a.name.toUpperCase();
  const bandB = b.name.toUpperCase();

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}

import React, { Component } from "react";

// Contains coe to display and handle linking of contacts to a client
export default class LinkContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }

  //attempt to link
  onSubmit = (e) => {
    e.preventDefault();
    if (this.state.email !== "")
      this.props.linkContact(this.props.client.code, this.state.email);
  };

  //update data values from input fields
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // JSX to render
  render() {
    return this.props.client.name != null ? (
      <div>
        <h2>Link a Conatct to {this.props.client.name}</h2>
        <form onSubmit={this.onSubmit}>
          <input
            placeholder="Email of contact"
            name="email"
            onChange={this.onChange}
          ></input>
          <button type="submit">Link</button>
        </form>
        <div>{this.props.message}</div>
      </div>
    ) : (
      <div>No client. Please select one from General.</div>
    );
  }
}

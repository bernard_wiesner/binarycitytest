import React, { Component } from "react";

// The client form from which a client can be found or added
export default class ClientForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      code: props.code,
      message: props.message,
      isNew: props.isNew,
    };
  }

  componentWillReceiveProps(props) {
    console.log(props);
    this.setState({ message: props.message, isNew: props.isNew });
    console.log(this.state.message);
  }

  // find a client
  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ code: "" });
    var ret = this.props.findClient(this.state.name, this.state.code);
    if (ret != null) this.setState({ code: ret });
  };

  // save a client
  onClick = (e) => {
    e.preventDefault();
    var code = this.props.generateCode(this.state.name.toUpperCase());
    console.log(code);
    this.props.addClient(this.state.name, code);
    console.log("Should Reset");
    this.setState({
      isNew: false,
      message: "",
      code,
    });
  };

  //update data values from input fields
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // jsx render for the page
  render() {
    const isEnabled = this.state.name.length;
    return (
      <div>
        <h4>Look for a client or save a new one</h4>
        <form onSubmit={this.onSubmit}>
          <div>
            <input
              type="text"
              placeholder="Name"
              value={this.state.name}
              onChange={this.onChange}
              name="name"
            ></input>
          </div>
          <div>
            <input
              type="text"
              readOnly={true}
              value={this.state.code}
              placeholder={this.state.code}
            ></input>
          </div>
          <button type="submit" disabled={!isEnabled}>
            Submit
          </button>
        </form>
        <div>{this.state.message}</div>
        <div>
          {this.state.isNew ? (
            <button onClick={this.onClick}>Save</button>
          ) : null}
        </div>
      </div>
    );
  }
}

import React from "react";

// The list containing all the clients in the db
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
  TablePagination,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// Column names
const columns = [
  {
    id: "name",
    align: "left",
    label: "Name",
    minWidth: 100,
    format: (value) => value.toLocaleString(),
  },
  {
    id: "code",
    align: "left",
    label: "Client Code",
    minWidth: 100,
    format: (value) => value.toLocaleString(),
  },
  {
    id: "num",
    label: "Num Contacts",
    minWidth: 50,
    align: "center",
    format: (value) => value.toFixed(0),
  },
];

//table style
const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
});

// Helper function to create the data
function createData(name, code, num) {
  return { name, code, num };
}

//The JSX component
export default function ClientList({ list }) {
  const rows = [];
  const classes = useStyles();

  // Create data from input list and put into rows
  for (var i = 0; i < list.length; i++) {
    var c = list[i]; //current client
    rows.push(createData(c.name, c.code, c.num));
  }

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // The actual JSX returned
  return list.length === 0 ? (
    <div> No client(s) found</div>
  ) : (
    <div>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

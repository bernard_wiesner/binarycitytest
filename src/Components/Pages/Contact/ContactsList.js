import React from "react";
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
  TablePagination,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

//Columns names
const columns = [
  {
    id: "name",
    align: "left",
    label: "Name",
    minWidth: 100,
    format: (value) => value.toLocaleString(),
  },
  {
    id: "surname",
    align: "left",
    label: "Surname",
    minWidth: 100,
    format: (value) => value.toLocaleString(),
  },
  {
    id: "email",
    label: "Email",
    minWidth: 170,
    align: "left",
    format: (value) => value.toLocaleString(),
  },
  {
    id: "num",
    label: "Num Clients",
    minWidth: 50,
    align: "center",
    format: (value) => value.toFixed(0),
  },
];

//style
const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
});

//Helper function to create rows
function createData(name, surname, email, num) {
  return { name, surname, email, num };
}

//JSX function component that renders a tbale containing all contacts from db
export default function ContactsList({ list }) {
  //order list according to spec
  list.sort(compare);
  const rows = [];
  const classes = useStyles();

  //create rows
  for (var i = 0; i < list.length; i++) {
    var c = list[i]; //current contact
    rows.push(createData(c.name, c.surname, c.email, c.num));
  }

  //Table page state management
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // JSX to render
  return list.length === 0 ? (
    <div>No Contact(s) Found</div>
  ) : (
    <div>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

// Function to help sort a list of contacts by [surname][name]
function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const surA = a.surname.toUpperCase();
  const surB = b.surname.toUpperCase();
  const nameA = a.name.toUpperCase();
  const nameB = b.name.toUpperCase();

  if (surA === surB) {
    return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
  } else {
    return surA > surB ? 1 : -1;
  }
}

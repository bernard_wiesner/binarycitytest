import React, { Component } from "react";

// Code to render and handle creation of contacts
export default class CreateContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      surname: "",
      email: "",
      message: props.message,
    };
  }

  // save contact
  onSubmit = (e) => {
    e.preventDefault();
    if (this.validateEmail(this.state.email))
      this.props.addContact(
        this.state.name,
        this.state.surname,
        this.state.email
      );
  };

  //update data from input fields
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentWillReceiveProps(props) {
    this.setState({ message: props.message });
  }

  //Regex to validate email input
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var result = re.test(String(email).toLowerCase());
    if (!result) {
      this.setState({ message: "Email not valid" });
    } else {
      this.setState({ message: "" });
    }
    return result;
  }

  //JSX to render
  render() {
    const { name, surname, email } = this.state;
    const isEnabled = name.length > 0 && surname.length > 0 && email.length > 0;
    return (
      <div>
        <h4> Create a contact </h4>
        <form onSubmit={this.onSubmit}>
          <div>
            <input
              type="text"
              placeholder="Name"
              onChange={this.onChange}
              name="name"
            ></input>
          </div>
          <div>
            <input
              type="text"
              placeholder="Surname"
              onChange={this.onChange}
              name="surname"
            ></input>
          </div>
          <div>
            <input
              type="text"
              placeholder="Email"
              onChange={this.onChange}
              name="email"
            ></input>
          </div>
          <button type="submit" disabled={!isEnabled}>
            Save
          </button>
        </form>
        <div>{this.state.message}</div>
      </div>
    );
  }
}

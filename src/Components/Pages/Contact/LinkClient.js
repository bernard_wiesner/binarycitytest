import React, { Component } from "react";

// Handles linking of contact to a client
export default class LinkClient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
    };
  }

  // create link
  onSubmit = (e) => {
    e.preventDefault();
    if (this.state.code !== "")
      this.props.linkClient(this.state.code, this.props.contact.email);
  };

  //update local data from input fields
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  //JSX to render
  render() {
    return this.props.contact.name != null ? (
      <div>
        <h2>
          Link a Client to {this.props.contact.name}{" "}
          {this.props.contact.surname}
        </h2>
        <form onSubmit={this.onSubmit}>
          <input
            placeholder="Client Code"
            name="code"
            onChange={this.onChange}
          ></input>
          <button type="submit">Link</button>
        </form>
        <div>{this.props.message}</div>
      </div>
    ) : (
      <div>No contact. Please select one from General.</div>
    );
  }
}

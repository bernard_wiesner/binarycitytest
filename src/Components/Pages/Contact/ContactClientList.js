import React from "react";
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
  TablePagination,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// column names
const columns = [
  {
    id: "name",
    align: "left",
    label: "Name",
    minWidth: 100,
    format: (value) => value.toLocaleString(),
  },
  {
    id: "code",
    label: "Client Code",
    minWidth: 170,
    align: "center",
    format: (value) => value.toLocaleString(),
  },
];

//style
const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
});

// helper function to create rows
function createData(name, code) {
  return { name, code };
}

//Renders a list of clients associated with a contact
export default function ContactClientList({ list }) {
  //sort list
  list.sort(compare);
  const rows = [];
  const classes = useStyles();

  //create rows
  for (var i = 0; i < list.length; i++) {
    var c = list[i]; //current client
    rows.push(createData(c.name, c.code));
  }

  //tbale page state management
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  //JSX to render
  return list.length === 0 ? (
    <div>No Clients Found</div>
  ) : (
    <div>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

// Function to sort clients by name as per spec
function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const bandA = a.code.toUpperCase();
  const bandB = b.code.toUpperCase();

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}

import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import ContactForm from "./ContactForm";
import ContactsList from "../Contact/ContactsList";
import ContactClientList from "./ContactClientList";
import CreateContact from "./CreateContact";
import LinkClient from "./LinkClient";

// The main view for contacts
export default class ContactView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: props.contacts,
      clients: [],
      contact: {},
      message: "",
      findMessage: "",
    };
  }

  componentWillReceiveProps(props) {
    this.setState({ props });
  }

  // Calls function from App.js to put a contact to th edb
  putDataToDB = (name, surname, email) => {
    console.log(name + " - " + surname + " - " + email);
    if (this.findContactsData(email)) {
      this.setState({ message: "Email already exists" });
    } else {
      var contacts = this.state.contacts;
      this.props.addContact(name, surname, email);
      this.setState({ contacts });
    }
  };

  // Calls a function from App.js to find all clients associated with the contact
  findContactsData = (email) => {
    for (var i = 0; i < this.state.contacts.length; i++) {
      var contact = this.state.contacts[i];
      if (contact.email === email) {
        var clients = this.props.getClients(contact);
        this.setState({
          findMessage: "Contact found!",
          clients,
          contact,
        });
        console.log("Found");
        return true;
      }
    }
    console.log("Not Found");
    this.setState({ findMessage: "No contact with those details found" });
    return false;
  };

  // JSX to render
  render() {
    return (
      <div>
        <h1>Contact View</h1>
        <CreateContact
          addContact={this.putDataToDB}
          message={this.state.message}
        ></CreateContact>
        <h3>All Contacts</h3>
        <ContactsList list={this.state.contacts} />
        <hr></hr>
        <h1>Contact Form</h1>
        <Tabs>
          <TabList>
            <Tab>General</Tab>
            <Tab>Clients</Tab>
            <Tab>Link</Tab>
          </TabList>

          <TabPanel>
            <ContactForm
              findContactsData={this.findContactsData}
              message={this.state.findMessage}
            />
          </TabPanel>
          <TabPanel>
            <ContactClientList list={this.state.clients} />
          </TabPanel>
          <TabPanel>
            <LinkClient
              contact={this.state.contact}
              linkClient={this.props.linkClient}
              message={this.props.linkMessage}
            ></LinkClient>
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

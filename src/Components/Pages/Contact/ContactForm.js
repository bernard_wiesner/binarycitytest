import React, { Component } from "react";

// Handles finding of contacts
export default class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      surname: "",
      email: "",
      message: props.message,
    };
  }

  // calls a function from App to find clients associated with the contact
  onSubmit = (e) => {
    e.preventDefault();
    if (this.validateEmail(this.state.email))
      this.props.findContactsData(this.state.email);
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentWillReceiveProps(props) {
    this.setState({ message: props.message });
  }

  //Regex to check for a valid email
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var result = re.test(String(email).toLowerCase());
    if (!result) {
      this.setState({ message: "Email not valid" });
    } else {
      this.setState({ message: "" });
    }
    return result;
  }

  // JSX to render
  render() {
    return (
      <div>
        <h4> Find a contact </h4>
        <form onSubmit={this.onSubmit}>
          <div>
            <input
              type="text"
              placeholder="Name"
              onChange={this.onChange}
              name="name"
            ></input>
          </div>
          <div>
            <input
              type="text"
              placeholder="Surname"
              onChange={this.onChange}
              name="surname"
            ></input>
          </div>
          <div>
            <input
              type="text"
              placeholder="Email"
              onChange={this.onChange}
              name="email"
            ></input>
          </div>
          <button type="submit"> Find Contact </button>
        </form>
        <div>{this.state.message}</div>
      </div>
    );
  }
}

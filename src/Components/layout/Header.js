import React from "react";
import { Link } from "react-router-dom";

//This is the persistent header at the top of the page
// it contains two links to the client and contact views
// as well as some styling
function Header() {
  return (
    <header style={headerStyle}>
      <h1>Test App</h1>
      <Link style={linkStyle} to="/client">
        Client
      </Link>{" "}
      |{" "}
      <Link style={linkStyle} to="/contact">
        Contacts
      </Link>
    </header>
  );
}

const headerStyle = {
  background: "#333",
  color: "#fff",
  textAlign: "center",
  padding: "10px",
};

const linkStyle = {
  color: "#fff",
  textDecoration: "none",
};

export default Header;

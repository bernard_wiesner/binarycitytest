const mongoose = require("mongoose");
const express = require("express");
var cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");
const ClientData = require("../models/ClientModel");
const ContactData = require("../models/ContactModel");

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();

// this is our MongoDB database
const dbRoute =
  "mongodb+srv://bernardwiesner:test123456@cluster0-lbosh.gcp.mongodb.net/test?retryWrites=true&w=majority";

// connects our back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));

// checks if connection with the database is successful
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));

// get all clients from the db
router.get("/getClients", (req, res) => {
  ClientData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});

// get all contacts form the db
router.get("/getContacts", (req, res) => {
  console.log("getting contacts");
  ContactData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});

// update a client
router.post("/updateClient", (req, res) => {
  const { id, num } = req.body;
  console.log(num + " " + id);
  ClientData.findByIdAndUpdate(id, { num: num }, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

// update a contact
router.post("/updateContact", (req, res) => {
  const { id, num, codes } = req.body;
  console.log("UPDAAAAAAATTTEEEE: " + num + " " + codes);
  ContactData.findByIdAndUpdate(id, { num: num, codes: codes }, (err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

// add a client
router.post("/addClient", (req, res) => {
  let data = new ClientData();

  const { name, code } = req.body;
  data.name = name;
  data.code = code;
  data.num = 0;

  data.save((err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

// add a contact
router.post("/addContact", (req, res) => {
  let data = new ContactData();

  const { name, surname, email } = req.body;
  data.name = name;
  data.surname = surname;
  data.num = 0;
  data.email = email;
  data.codes = [];

  data.save((err) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

// append /api for our http requests
app.use("/api", router);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));

import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import "react-tabs/style/react-tabs.css";
import Header from "./Components/layout/Header";
import ClientView from "./Components/Pages/Client/ClientView";
import ContactView from "./Components/Pages/Contact/ContactView";
import axios from "axios";

//Top level Component for the web app
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clients: [],
      contacts: [],
      linkMessage: "",
    };
  }

  // when component mounts, first thing it does is fetch all existing data in our db
  // then we incorporate a polling logic so that we can easily see if our db has
  // changed and implement those changes into our UI
  componentDidMount() {
    this.getClientDataFromDb();
    this.getContactDataFromDb();
    if (!this.state.intervalIsSet) {
      this.getContactDataFromDb();
      let interval = setInterval(this.getClientDataFromDb, 1000);
      this.setState({ intervalIsSet: interval });
    }
  }

  // never let a process live forever
  // always kill a process everytime we are done using it
  componentWillUnmount() {
    if (this.state.intervalIsSet) {
      clearInterval(this.state.intervalIsSet);
      this.setState({ intervalIsSet: null });
    }
  }

  //Call server function to add client to db
  getClientDataFromDb = () => {
    fetch("http://localhost:3001/api/getClients")
      .then((data) => data.json())
      .then((res) => this.setState({ clients: res.data }));
  };

  //Call server function to add contact to db
  getContactDataFromDb = () => {
    fetch("http://localhost:3001/api/getContacts")
      .then((data) => data.json())
      .then((res) => this.setState({ contacts: res.data }));
  };

  // update a client - due to linking
  updateClient = (id, num) => {
    axios.post("http://localhost:3001/api/updateClient", {
      id: id,
      num: num,
    });
  };

  //update contact - due to linking
  updateContact = (id, num, codes) => {
    axios.post("http://localhost:3001/api/updateContact", {
      id: id,
      num: num,
      codes: codes,
    });
  };

  // calls server function to get all clients from db
  getClients(contact) {
    var ret = [];
    var codeList = contact.codes;
    var clients = this.state.clients;

    for (var i = 0; i < codeList.length; i++) {
      for (var j = 0; j < clients.length; j++) {
        if (clients[j].code === codeList[i]) {
          ret.push(clients[j]);
          break;
        }
      }
    }
    return ret;
  }

  // calls server function to get all contacts from db
  getContacts(code) {
    console.log("looking for contacts");
    var ret = [];
    var contacts = this.state.contacts;
    console.log("Contacts" + contacts.length);
    for (var i = 0; i < contacts.length; i++) {
      var contact = contacts[i];
      console.log("contact codes", contact.codes);
      console.log("code", code);
      if (contact.codes.includes(code)) {
        ret.push(contact);
      }
    }
    console.log(ret);
    return ret;
  }

  // Function to help create a link - passed to sub-components
  linkClientToContact(code, email) {
    console.log("Trying to link");
    var contacts = this.state.contacts;
    var clients = this.state.clients;
    console.log(contacts);
    for (var i = 0; i < contacts.length; i++) {
      var contact = contacts[i];
      if (contact.email === email) {
        console.log("Founnd email match");
        if (contact.codes.includes(code)) {
          this.setState({ linkMessage: "Contact already linked to client" });
          return false;
        }
        console.log(clients.length);
        for (var j = 0; j < clients.length; j++) {
          var client = clients[j];
          console.log("=====" + client.code + "===" + code);
          if (client.code === code) {
            client.num++;
            this.updateClient(client._id, client.num);
            console.log("Updated client counter");
            break;
          }
        }
        contact.codes.push(code);
        contact.num++;
        this.updateContact(contact._id, contact.num, contact.codes);
        this.setState({ linkMessage: "Contact succefully linked" });
        console.log("LINKED: " + code + " " + email);
        return true;
      }
    }
    this.setState({ linkMessage: "No contact or client found" });
    return false;
  }

  //calls server function to save a new client
  addClient(name, code) {
    axios.post("http://localhost:3001/api/addClient", {
      name,
      code,
    });
  }

  //calls server function to save a new contact
  addContact(name, surname, email) {
    axios.post("http://localhost:3001/api/addContact", {
      name,
      surname,
      email,
    });
  }

  // Generates the a code for a new client
  generateCode(name) {
    name = name.toUpperCase();
    var splitName = name.split(" "); //To check for words
    var available = [];

    if (splitName.length > 2) {
      // 3 or more words in name
      available.push(splitName[0][0], splitName[1][0], splitName[2][0]);
    } else if (splitName.length === 2) {
      //2 words in name
      available.push(splitName[0][0], splitName[1][0]);
    } else {
      // single word
      if (name.length > 2) {
        // word has 3 or more characters
        available.push(name[0], name[1], name[2]);
      } else if (name.length === 2) {
        // word has 2 characters
        available.push(name[0], name[1]);
      } else {
        // word only has 1 character (guaranteed by input validation on fields)
        available.push(name[0]);
      }
    }

    //Call recursive function to build code
    return buildCode(available, this.state.clients, 0, "");
  }

  //JSX to render
  render() {
    return (
      <Router>
        <div>
          <Header />
          {/* <Route
          exact
          path="/"
          render={(props) => {
            <div></div>;
          }}
        /> */}
          <Route
            path="/client"
            render={(props) => (
              <ClientView
                clients={this.state.clients}
                getContacts={this.getContacts.bind(this)}
                linkContact={this.linkClientToContact.bind(this)}
                linkMessage={this.state.linkMessage}
                addClient={this.addClient}
                generateCode={this.generateCode.bind(this)}
              />
            )}
          />
          <Route
            path="/contact"
            render={(props) => (
              <ContactView
                contacts={this.state.contacts}
                getClients={this.getClients.bind(this)}
                contactClients={this.state.contactClients}
                linkClient={this.linkClientToContact.bind(this)}
                linkMessage={this.state.linkMessage}
                addContact={this.addContact}
              />
            )}
          />
        </div>
      </Router>
    );
  }
}

//Helper function to recursively build a code from a list of given available chars to include
function buildCode(available, currentList, length, code) {
  console.log(available + " | " + currentList + " | " + length + " | " + code);
  // Alpha section
  if (length < 3) {
    var nextC = available.shift();
    if (nextC === undefined) nextC = "A"; //Availible list empty ( less than 3 characters or words in name)
    do {
      var prefix = code + nextC; //prefix for filter search

      //filter those in list that starts with prefix
      var filteredList = currentList.filter((client) =>
        client.code.startsWith(prefix)
      );

      if (filteredList.length === 0) {
        //Fill in the rest of the available chars
        for (var c of available) {
          if (length === 3) break;
          prefix += c;
          length++;
        }
        // fill any remaining space with 'A'
        for (var i = 0; i < 3 - length - 1; i++) {
          prefix += "A";
        }
        //Add numeric part
        return prefix + "001";
      }

      // move to next char
      var val = buildCode(available, filteredList, length + 1, prefix);
      if (val.length >= 6) return val; //Error or code generated

      nextC = nextChar(nextC); //no code with current prefix found - move to next char
    } while (nextC !== "Z");
    return [];
  } else {
    //Numeric section - no need for recursion
    if (currentList.length === 0) return code + "001";
    var numericVal = parseInt(currentList.pop().code.substr(6 - 3));
    // check if numeric part is the last value available in current domain
    if (numericVal === 999) {
      //All codes exhausted
      if (code + numericVal === "ZZZ999") {
        return "No more codes available in namespace";
      } else {
        //Return failure to find a suitable code
        return [];
      }
    } else {
      //simply increment last numeric value of last entry
      numericVal++;
      return code + ("0000" + numericVal).slice(-3);
    }
  }
}

// Helper function to incrment a char by ascii value
function nextChar(c) {
  return String.fromCharCode(c.charCodeAt(0) + 1);
}

// Fucntion to help sort clients by their codes
function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const bandA = a.code.toUpperCase();
  const bandB = b.code.toUpperCase();

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}

export default App;
